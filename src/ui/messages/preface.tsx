import React, { MutableRefObject } from "react";
import { IPreface, L10n } from "tripetto-runner-foundation";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { markdownToJSX } from "@helpers/markdown";
import { isIn } from "@helpers/isin";
import { IViewport } from "@interfaces/viewport";
import { ButtonFabric } from "tripetto-runner-fabric/components/button";
import { Block, Blocks } from "@ui/blocks";
import { BlockButtons } from "@ui/blocks/buttons";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockDescription } from "@ui/blocks/description";
import { BlockVideo } from "@ui/blocks/video";
import { Banner } from "@ui/messages/banner";
import { RightIcon } from "@ui/icons/right";

export const Preface = (
    props: IPreface & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly viewportRef: MutableRefObject<IViewport>;
        readonly isPage: boolean;
        readonly start: () => void;
        readonly edit?: () => void;
    }
) => (
    <Blocks props={{ ...props, isMessage: true, center: true }}>
        <Block styles={props.styles} view={props.view} isPage={props.isPage} isMessage={true}>
            {props.image && <BlockImage src={props.image} isPage={props.isPage} alignment="center" onClick={props.edit} />}
            {props.title && (
                <BlockTitle props={{ hasDescription: true, alignment: "center" }} onClick={props.edit}>
                    {markdownToJSX(props.title)}
                </BlockTitle>
            )}
            {props.description && (
                <BlockDescription props={{ alignment: "center" }} onClick={props.edit}>
                    {markdownToJSX(props.description)}
                </BlockDescription>
            )}
            {props.video && <BlockVideo src={props.video} onClick={props.edit} />}
            <BlockButtons styles={props.styles} alignment="center">
                <ButtonFabric
                    styles={props.styles.buttons}
                    icon={RightIcon}
                    label={props.button || props.l10n.pgettext("runner#1|🆗 Buttons", "Start")}
                    onClick={props.start}
                    onAutoFocus={(element) => {
                        if (
                            props.view === "live" &&
                            (props.styles.autoFocus || props.isPage) &&
                            element &&
                            isIn(element, props.viewportRef)
                        ) {
                            element.focus({
                                preventScroll: true,
                            });
                        }
                    }}
                />
            </BlockButtons>
            <Banner l10n={props.l10n} styles={props.styles} view={props.view} alignment="center" />
        </Block>
    </Blocks>
);
