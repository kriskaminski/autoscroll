import styled from "styled-components";

export const BlockEnumerator = styled.span`
    font-size: 14px;
    font-weight: normal;
    line-height: 1em;
    display: inline-block;
    width: 32px;
    margin-left: -32px;
    text-align: center;
`;
