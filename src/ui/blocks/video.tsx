import React from "react";
import styled from "styled-components";

const getYouTubeId = (url: string) => {
    const videoID = url.match(/youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/);

    return (videoID && videoID.length === 2 && videoID[1]) || "";
};

const getVimeoId = (url: string) => {
    const videoID = url.match(/\/\/(?:www\.)?vimeo\.com\/(?:channels\/staffpicks\/)?([-\w]+)/i);

    return (videoID && videoID.length === 2 && videoID[1]) || "";
};

export const BlockVideoElement = styled.div`
    margin: 0;
    padding: 0;
    border: none;
    width: 100%;
    height: calc(100vw * 9 / 16);
    max-width: 100%;

    > iframe {
        width: 100%;
        height: 100%;
        border: none;
    }
`;

export const BlockVideo = (props: { readonly src: string; readonly onClick?: () => void }) => {
    const youTubeId = getYouTubeId(props.src);
    const vimeoId = getVimeoId(props.src);

    return youTubeId || vimeoId ? (
        <BlockVideoElement onClick={props.onClick}>
            {youTubeId && (
                <iframe
                    src={"https://www.youtube-nocookie.com/embed/" + youTubeId}
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                />
            )}
            {vimeoId && <iframe src={"https://player.vimeo.com/video/" + vimeoId} allow="autoplay" />}
        </BlockVideoElement>
    ) : (
        <></>
    );
};
