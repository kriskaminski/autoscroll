import { mountNamespace, unmountNamespace } from "tripetto-runner-foundation";
import { namespace } from "@namespace";

mountNamespace(namespace);

export { AutoscrollRunner } from "./autoscroll";
export { IAutoscrollProps, TAutoscrollPause } from "@interfaces/props";
export { IAutoscrollSnapshot } from "@interfaces/snapshot";
export { IAutoscrollRunner } from "@interfaces/runner";
export { IAutoscrollStyles } from "@interfaces/styles";
export { IAutoscrollController } from "@hooks/controller";
export { IAutoscrollRendering, IAutoscrollRenderProps } from "@interfaces/block";
export { run } from "./run";
export { namespace } from "./namespace";

unmountNamespace();
