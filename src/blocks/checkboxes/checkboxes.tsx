import React from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Checkboxes } from "tripetto-block-checkboxes/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { CheckboxesFabric } from "tripetto-runner-fabric/components/checkboxes";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-checkboxes",
})
export class CheckboxesBlock extends Checkboxes implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): React.ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <CheckboxesFabric
                    styles={props.styles.checkboxes}
                    view={props.view}
                    checkboxes={this.props.checkboxes.map((checkbox) => ({
                        label: checkbox.name,
                        value: this.checkboxSlot(checkbox),
                        tabIndex: props.tabIndex,
                    }))}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus()}
                    onFocus={props.focus()}
                    onBlur={props.blur()}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
