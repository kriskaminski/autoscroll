import React from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Matrix } from "tripetto-block-matrix/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { MatrixFabric } from "tripetto-runner-fabric/components/matrix";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-matrix",
})
export class MatrixBlock extends Matrix implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): React.ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <MatrixFabric
                    styles={props.styles.matrix}
                    columns={this.props.columns}
                    rows={this.props.rows.map((row) => {
                        const value = this.valueOf<string>(row.id);
                        const required = !this.props.required && value && value.slot.required ? true : false;

                        return {
                            ...row,
                            required,
                            value,
                            tabIndex: props.tabIndex,
                        };
                    })}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={true}
                    onAutoFocus={props.autoFocus()}
                    onFocus={props.focus()}
                    onBlur={props.blur()}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
