import React from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { MultipleChoice } from "tripetto-block-multiple-choice/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockCaption } from "@ui/blocks/caption";
import { markdownToJSX } from "@helpers/markdown";
import { MultipleChoiceFabric } from "tripetto-runner-fabric/components/multiple-choice";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-multiple-choice",
    alias: "multiple-choice",
    autoRender: true,
})
export class MultipleChoiceBlock extends MultipleChoice implements IAutoscrollRendering {
    readonly hideButtons = !this.props.multiple;
    readonly autoSubmit = !this.props.multiple;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): React.ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={this.props.imageURL} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {this.props.caption && <BlockCaption>{markdownToJSX(this.props.caption, this.context)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={this.props.imageURL} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <MultipleChoiceFabric
                    styles={props.styles.multipleChoice}
                    view={props.view}
                    buttons={this.props.choices.map((choice) => ({
                        ...choice,
                        slot: (this.props.multiple && this.valueOf(choice.id)) || undefined,
                        tabIndex: props.tabIndex,
                    }))}
                    alignment={(this.props.alignment && "horizontal") || "vertical"}
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus()}
                    onFocus={props.focus()}
                    onBlur={props.blur()}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
