import React from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Paragraph } from "tripetto-block-paragraph/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockCaption } from "@ui/blocks/caption";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";
import { markdownToJSX } from "@helpers/markdown";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-paragraph",
    alias: "paragraph",
})
export class ParagraphBlock extends Paragraph implements IAutoscrollRendering {
    readonly autoFocus = true;

    render(props: IAutoscrollRenderProps): React.ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={this.props.imageURL} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {this.props.caption && <BlockCaption>{markdownToJSX(this.props.caption, this.context)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={this.props.imageURL} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.video && <BlockVideo src={this.props.video} />}
            </>
        );
    }
}
