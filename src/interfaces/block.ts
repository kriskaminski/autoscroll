import { L10n, NodeBlock } from "tripetto-runner-foundation";
import { FocusEvent } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { IRunnerAttachments, TRunnerViews } from "tripetto-runner-react-hook";

export interface IAutoscrollRenderProps {
    readonly index: number;
    readonly id: string;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly name: JSX.Element | undefined;
    readonly description: JSX.Element | undefined;
    readonly explanation: JSX.Element | undefined;
    readonly label: JSX.Element | undefined;
    readonly placeholder: string;
    readonly tabIndex: number;
    readonly isFailed: boolean;
    readonly isPage: boolean;
    readonly ariaDescribedBy: string | undefined;
    readonly ariaDescription: JSX.Element | undefined;
    readonly focus: <T>(onFocus?: (e: FocusEvent) => T) => (e: FocusEvent) => T;
    readonly blur: <T>(onBlur?: (e: FocusEvent) => T) => (e: FocusEvent) => T;
    readonly autoFocus: (onAutoFocus?: (element: HTMLElement) => void) => (element: HTMLElement | null) => void;
    readonly attachments: IRunnerAttachments | undefined;
}

export interface IAutoscrollRendering extends NodeBlock {
    readonly required?: boolean;
    readonly hideRequiredAsteriskFromName?: boolean;
    readonly hideAriaDescription?: boolean;
    readonly hideButtons?: boolean;
    readonly autoSubmit?: boolean;
    readonly autoFocus?: boolean;
    readonly render?: (props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void) => React.ReactNode;
}
