const fs = require("fs");
const tripetto = require("tripetto");

function Alias(path, base) {
    const files = fs.readdirSync(path) || [];

    files.forEach(function (file) {
        if (fs.statSync(path + file).isDirectory()) {
            Alias(path + file + "/", base + "../");
        } else if (fs.statSync(path + file).isFile() && file.lastIndexOf(".d.ts") === file.length - 5) {
            let code = fs.readFileSync(path + file, "utf8");

            code = tripetto.Str.replace(code, `import "@blocks";`, "");
            code = tripetto.Str.replace(code, `"@blocks`, `"${base || "./"}blocks`);
            code = tripetto.Str.replace(code, `"@helpers`, `"${base || "./"}helpers`);
            code = tripetto.Str.replace(code, `"@hooks`, `"${base || "./"}hooks`);
            code = tripetto.Str.replace(code, `"@interfaces`, `"${base || "./"}interfaces`);
            code = tripetto.Str.replace(code, `"@styles`, `"${base || "./"}styles`);
            code = tripetto.Str.replace(code, `"@ui`, `"${base || "./"}ui`);

            fs.writeFileSync(path + file, code, "utf8");
        }
    });
}

Alias("./runner/types/bundle/src/", "");
