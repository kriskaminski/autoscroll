const fs = require("fs");
const tripetto = require("tripetto");
const banner = require("../banner/banner.js");
const path = "./runner/types/index.d.ts";
const package = require("../../package.json");

let types = fs.readFileSync(path, "utf8");
let comment = types.indexOf("\n//");

while (comment !== -1) {
    const end = types.substr(comment + 1).indexOf("\n");

    if (end === -1) {
        break;
    }

    types = types.substr(0, comment) + types.substr(comment + 1 + end);
    comment = types.indexOf("\n//");
}

types = tripetto.Str.replace(types, "/*! BANNER */", `/*! ${banner} */`);
types = tripetto.Str.replace(types, `import("./styles").`, `import("${package.name}/hooks/styles").`);

fs.writeFileSync(path, types, "utf8");
